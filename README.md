JABS
====

Jest + Angular + Browserify + Sass... w/ Gulp :)

The aim of this repo is to create a seed project that includes Angular, Jest (Testing Framework), Browserify, Sass with Gulp. You should be able to run the commands below and have it working. The directory structure is not set in stone, move things around where you see fit.

Requirements:
--------------
    nodejs
    bower (OPTIONAL if you want to add any client side only js scripts)
    http-server (web server that's easy to install and use)

Installation and getting it up and running:
-------------------------------------------
    1. git clone <this repo>
    2. npm install
    3. gulp build
    4. gulp watch  (watches the js and sass files for any changes)
    5. http-server (if you are using http-server)

Gulp Tasks
--------------
    1. test (runs all your tests in the __tests__ directory)
    2. css ("compiles" all of your sass files to 1 minified css file -> vendor.min.css)
    3. vendor_css ("compiles" the bootstrap css files into 1 minified css file -> bundle.min.css)
    3. js ("compiles" your js files to 1 minified js file -> bundle.min.js)
    4. lint (lints all of your js files using jshint)

W/ PHP
======

Laravel 5:
--------------
Here is the layout I've used in conjunction with Laravel 5. I've placed the markup from the *index.html* to wherever Laravel's *hello.php* file is as well as updated the references in the *gulpfile.js* file. Note: instead of using *http-serve* you'll probably be doing *php artisian serve* to run your code...if that wasn't already obvious :)

    public/
        assets/
            js/
            img/
            fonts/
            partials/
            css/
    resources/
        __tests__/
        sass/
        js/
        bower.json
        gulpfile.js
        package.json

Codeigniter 2/3
-------------------
Here is the layout I've used in my codeigniter projects. Rename directories and paths where you see fit. I usually move my index.php file into a directory called "public" and then point my web root for apache or nginx to it then serve all my static content from there. You can also use PHP's built in webserver by doing "php -S localhost:8080" from inside the public directory

    system/
    application/
    public/
        index.php
        assets/
            js/
            img/
            fonts/
            partials/
            css/
    resources/
        sass/
        js/
        __tests__/
        bower.json
        gulpfile.js
        package.json


Here is what I added to *gulpfile.js* file in the top requires section, you may have to update your package.json file to include gulp-phpunit and whatever else you will be using for php:

    exec = require('child_process').exec,
    gexec = require('gulp-exec'),
    sys = require('sys'),
    phpunit = require('gulp-phpunit');

Then in the tasks:

    //////////////////////////////////////////////////////////////////
    //OTHER LANGUAGE COMMANDS (can be used in the watch)
    //////////////////////////////////////////////////////////////////
    //phpunit
    gulp.task("phpunit", function () {
        var options = {debug: false, notify: true};
        gulp.src('app/tests/*.php')
            .pipe(phpunit('phpunit', options))
            .on('error', notify.onError({
                title: "Failed Tests!",
                message: "Error(s) occurred during testing..."
            }));
    });

    //php -l (lint)
    gulp.task("phplint", function () {
        //lint controllers
        gulp.src("app/controllers/**/*.php")
            .pipe(gexec("php -l <%= file.path %>"))
            .pipe(gexec.reporter());
        //lint other specific directories
    });

    //php code sniffer
    gulp.task("phpcs", function () {
        //you get the idea
        //exec("some awesome program for php code sniffing");
    });

    //etc...

Within the "Watch" task:

    gulp.watch("app/**/*.php", ["phpunit", "phplint", "phpcs"]);

Some of the php tasks in the watch might be overkill for each change in some php file, so add and remove as you see fit.