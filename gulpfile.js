var gulp = require("gulp"),
    browserify = require("browserify"),
    transform = require('vinyl-transform'),
    rename = require("gulp-rename"),
    uglify = require("gulp-uglify"),
    sass = require("node-sass"),
    notify = require("gulp-notify"),
    notifier = require("node-notifier"),
    concat = require('gulp-concat'),
    jshint = require('gulp-jshint'),
    shell = require('gulp-shell');

//////////////////////////////////////////////////////////////////
//JAVASCRIPT
//////////////////////////////////////////////////////////////////
//Custom JS
gulp.task("js", function () {
    var browserified = transform(function (filename) {
        var bFile = browserify(filename);
        return bFile.bundle();
    });
    return gulp.src("js/main.js")
        .pipe(browserified)
        .pipe(uglify({mangle:false}))
        .pipe(rename("dist/bundle.min.js"))
        .pipe(gulp.dest("./"))
        .pipe(notify("JS Compiled"));
});

//Lint Custom JS
gulp.task('lint', function() {
    return gulp.src('js/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

//Run JS Tests
gulp.task('test', shell.task([
    'npm test'
]));
//////////////////////////////////////////////////////////////////
//CSS/SASS
//////////////////////////////////////////////////////////////////
//Custom CSS/SASS
gulp.task("css", function () {
    sass.renderFile({
        file: 'sass/main.sass',
        outFile: 'dist/bundle.min.css',
        outputStyle: 'compressed',
        success: function () {
            notifier.notify({
                title: "SASS/CSS",
                message: "Sass compiled to CSS"
            });
        },
        error: function (error) {
            notifier.notify({
                title: "SASS/CSS Error",
                message: "Sass NOT Compiled to CSS: " + error
            });
        }
    });
});

//Vendor CSS
gulp.task("vendor_css", function () {
    gulp.src([
            'node_modules/bootstrap/dist/css/bootstrap.min.css',
            'node_modules/bootstrap/dist/css/bootstrap-theme.min.css'
        ])
        .pipe(concat("vendor.min.css"))
        .pipe(gulp.dest("dist/"))
        .pipe(notify("Vendor CSS Compiled"));
});

//////////////////////////////////////////////////////////////////
//WATCHERS
//////////////////////////////////////////////////////////////////
gulp.task("watch", function () {
    gulp.watch("js/**/*.js", ["lint", "test", "js"]);
    gulp.watch("sass/**/*.sass", ["css"]);
});

//////////////////////////////////////////////////////////////////
//DEFAULT
//////////////////////////////////////////////////////////////////
gulp.task("default", ['lint', 'js', 'css']);

gulp.task("build", ["css", "vendor_css", "lint", "test", "js"]);

gulp.task("develop", ["watch"]);
