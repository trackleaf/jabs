//include and don't mock angular
jest.dontMock("angular/angular");
require("angular/angular");

//include and don't mock angular-mocks
jest.dontMock("angular-mocks/angular-mocks");
require("angular-mocks/angular-mocks");

//don't mock these controllers
jest.dontMock("../js/controllers/welcomeController");

//WELCOME CONTROLLER
describe('Controller: welcomeController', function() {
    var scope, ctrl;

    beforeEach(function () {
        inject(function ($controller, $rootScope) {
            scope = $rootScope.$new();

            //include the controller
            ctrl = $controller(require("../js/controllers/welcomeController"), {
                $scope: scope
            });
        });
    });

    describe("Basic Test", function () {
        //Failing this on purpose to see what it looks like
        it('should have the correct number of tasks', function() {
            expect(scope.tasks.length).toEqual(3);
        });
    });

});
