//include and don't mock angular
jest.dontMock("angular/angular");
require("angular/angular");

//include and don't mock angular-mocks
jest.dontMock("angular-mocks/angular-mocks");
require("angular-mocks/angular-mocks");

//Dont' Mock these controllers
jest.dontMock("../js/controllers/homeController");

//HOME CONTROLLER
describe('Controller: homeController', function() {
    var scope, ctrl;

    beforeEach(function () {
        inject(function ($controller, $rootScope) {
            scope = $rootScope.$new();

            //include the controller
            ctrl = $controller(require("../js/controllers/homeController"), {
                $scope: scope
            });
        });
    });

    describe("Basic Tests", function () {
        it('should read "JABS - Jest, Angular, Browserify, Sass"', function() {
            expect(scope.name).toBe('JABS - Jest, Angular, Browserify, Sass');
        });

				it('should have 5 items in the tech array', function () {
					expect(scope.tech.length).toEqual(5);
				});
    });

});