require('angular');
require('angular-bootstrap/ui-bootstrap-tpls');

var controllers = angular.module('myApp.controllers',['ui.bootstrap', 'ui.bootstrap.tpls']);

//Controllers
controllers.controller('homeController', require('./controllers/homeController'));
controllers.controller('welcomeController', require('./controllers/welcomeController'));

//export the module
module.exports = controllers;
