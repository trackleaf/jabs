/*global angular */
require('angular');
require('angular-ui-router/release/angular-ui-router');

var myApp = angular.module('myApp', [
        'ui.router',
        'myApp.directives',
        'myApp.services',
        'myApp.resources',
        'myApp.filters',
        'myApp.controllers'
    ]).config(require('./routes'));

//Controllers
var controllers = require('./controllers');

//Directives
var directives = require('./directives');

//Filters
var filters = require('./filters');

//Services
var services = require('./services');

//Resources
var resources = require('./resources');
