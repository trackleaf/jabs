module.exports = function ($scope) {
    'use strict';
    $scope.name = 'JABS - Jest, Angular, Browserify, Sass';
    $scope.address = 'https://github.com/trackleaf/jabs';
    $scope.tasks = ['one', 'two', 'three'];
};
