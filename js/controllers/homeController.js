module.exports = function ($scope) {
    'use strict';
    $scope.name = 'JABS - Jest, Angular, Browserify, Sass';
    $scope.address = 'https://github.com/trackleaf/jabs';
		$scope.links = [
			{
				name: "Welcome Page",
				url: "#welcome"
			},
			{
				name: "Other Page",
				url: "#other"
			}
		];
    $scope.tech = [
        {
            name: "Jest",
            url: "https://github.com/facebook/jest",
            desc: "Painless JavaScript Unit Testing"
        },
        {
            name: "Angular",
            url: "https://github.com/angular/angular.js",
            desc: "AngularJS lets you write client-side web applications as if you had a smarter browser."
        },
        {
            name: "Browserify",
            url: "https://github.com/substack/node-browserify",
            desc: "Use a node-style require() to organize your browser code and load modules installed by npm."
        },
        {
            name: "Sass",
            url: "https://github.com/sass/node-sass",
            desc: "Node-sass is a library that provides binding for Node.js to libsass, the C version of the popular stylesheet preprocessor, Sass."
        },
        {
            name: "Bootstrap",
            url: "https://github.com/twbs/bootstrap",
            desc: "Bootstrap is a sleek, intuitive, and powerful front-end framework for faster and easier web development, created by Mark Otto and Jacob Thornton, and maintained by the core team with the massive support and involvement of the community."
        }
    ];
    $scope.tabs = [
        { 
            title:'Dynamic Title 1',
            content:'Dynamic tab created by angular-bootstrap'
        },
        { 
            title:'Dynamic Title 2',
            content:'2nd dynamic tab created by angular-bootstrap'
        }
    ];
};
