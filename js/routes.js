module.exports = function ($urlRouterProvider, $stateProvider) {
    'use strict';
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: './partials/home.html',
            controller: 'homeController'
        })
        .state('welcome', {
            url: '/welcome',
            templateUrl: './partials/welcome.html',
            controller: 'welcomeController'
        });
};
