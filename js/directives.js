require('angular');

var directives = angular.module('myApp.directives',[]);
directives.directive('searchBox', require('./directives/searchBox'));

module.exports = directives;
